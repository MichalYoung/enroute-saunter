#! /usr/bin/env python3
#
"""
Filter a GPX file to simplified.
Initial version uses fixed threshhold of 100 meters. 

(Command line tool, for route preparation)
"""

# Load modules from ./lib
import sys
import argparse
import gpxpy
import gpxpy.gpx


fname = "export.gpx"  # FIXME
delta = 100

def getargs():
    """Return arguments as a NameSpace object"""
    parser = argparse.ArgumentParser("Simplify GPX files")
    parser.add_argument('infile',
        type=argparse.FileType(mode='r', encoding="utf-8", errors="replace"), 
        help="The GPX input file")
    parser.add_argument('outfile', 
        type=argparse.FileType(mode='w', encoding="utf-8"),
        help="Output to this GPX file")
    argvals = parser.parse_args()
    return argvals
                               
def main():
    args = getargs()
    gpx = gpxpy.parse(args.infile)
    if delta:
        gpx.simplify(delta)
    print(gpx.to_xml(), file=args.outfile)

main()




