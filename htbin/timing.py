from time import perf_counter, strftime

EVENTS = [ ]
def log_time(evname):
    global EVENTS
    event = (evname, perf_counter())
    EVENTS.append(event)
    return

def report_times():
    global EVENTS
    begin_event, begin_time = EVENTS[0]
    prior = begin_time
    for event in EVENTS[1:]:
        evname, evtime = event
        delta = evtime - prior
        elapsed = evtime - begin_time
        prior = evtime
        print("{}\t{:3f}  ({:3f})".format(evname, elapsed, delta))

# log_time("begin")
# report_times()
