To make oath2client.client work, needed to install
     pip install pyOpenSSL

(otherwise "No crypto library available")

pip install gspread

Lynne's feed: 0VMqdoAtvZtPTNMNZJVZa5jhHdej3zLqM

env not finding Python3: 
   per https://gist.github.com/thebigdog/5208619

Solution: Add a new path of /usr/local/bin to Apache2's path where the
program was installed

Edit the Apache2 plist file with whatever editor you like (example
using vim):

$ sudo vim /System/Library/LaunchDaemons/org.apache.httpd.plist
Add the following to the the plist file:

<key>EnvironmentVariables</key>
<dict>
  <key>PATH</key>
  <string>/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin</string>
</dict>
but also add /usr/local/bin, and place python3 there. 

