"""
Obtain trace information from a spot tracker.
(For use in a cgi-bin script.)

"""

import urllib.request
import json


URL_API = "https://api.findmespot.com/spot-main-web/consumer/rest-api/2.0/public/feed/"
URL_REQUEST_RECENT = "/message"

def feed(feed_id,empty_exception=False):
    """
    Retrieve the last 50 messages from feed.
    Args:
        feed_id:  A spot feed identifier
    Returns:
        list of 'message' objects.  Each message is a dict,
        with attributes including "latitude" and "longitude".

        Note list may be empty if there are no points to retrieve,
        unless empty_exception is set to True, in which case an
        exception is raised with the Spot error message. 
    """
    URL= URL_API + feed_id + URL_REQUEST_RECENT
    response = urllib.request.urlopen(URL)
    txt = response.read().decode("utf-8")
    data=json.loads(txt)
    if "errors" in data["response"]:
        if empty_exception:
            msg = data["response"]["errors"]["error"]["description"]
            raise Exception(msg)
        else:
            return [ ]

    points = data["response"]["feedMessageResponse"]["messages"]["message"]
    if isinstance(points,list):
        return points
    else:
        # Spot tracker seems to special-case the singleton
        return [ points ]


def msg_to_latlon(pt):
    """
    From one Spot message to a latitude, longitude pair.
    Args:
       pt:  A dict containing 'lattitude' and 'longitude' attributes,
            e.g., one of the elements in the list returned by feed(..).
    Returns:
       tuple (lattitude,longitude)
    """
    return ( pt["latitude"], pt["longitude"] )


    
