#! /usr/bin/env python3
#
import spot
import sys
import cgi
import json
import cgitb
# import datetime
from time import sleep
cgitb.enable()


form = cgi.FieldStorage()
feedlist = form.getlist('feed')

# print("Feeds requested: {}".format(feedlist), file=log)

featurelist = [ ]
geojson = { "type": "FeatureCollection",
            "features": featurelist
            }

for feed in feedlist:
    sleep(2)
    points = spot.feed(feed)
    if points == [ ] : continue  # Skip unavailable feeds 
    latest = points[0]

    # Trajectory over a given period or number of observations ... number 
    # of observations for now.
    trajectory = [ ]
    if len(points) > 1: 
        recent = points[0:6]  # Could be less than 6 observations
        for point in recent: 
            trajectory.append( [ point["latitude"], point["longitude"]]  )
        feature = { "type": "Feature", 
                    "geometry": {
                          "type": "LineString",
                           "coordinates": trajectory
                     }, 
                     "properties": {
                          "id": feed, 
                          "show_on_map": "true"
             }
        }
        featurelist.append(feature)  
    
    feature = { "type": "Feature",
                "geometry": {
                    "type": "Point",
                    "coordinates" : [ latest["latitude"], latest["longitude"] ],
                    },
                "properties": {
                    "id": feed,
                    "observed_time": latest["dateTime"], 
                    "show_on_map": "true",
                    "name": "Spotted", 
                  }
                }
    featurelist.append(feature)

#print("Featurelist returned: {}".format(geojson),file=log)

print("Content-type: text/plain")
print("")
print(json.dumps(geojson))

