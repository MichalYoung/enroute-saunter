In this directory: Small experiments that I don't want to clutter up
the htbin directory with.  These are typically Python code that is
not invoked through the cgi-bin interface; when they are ready to
be invoked through the web interface, we promote them to the
htbin directory.
