#! /usr/bin/env python3
#
"""
Retrieval of a list of points from a URL,
with optional simplification.

Performance note:  With Ride With GPS as data source,
dominant cost is download of raw GPX data.  For a GPX
route with 3594 points, download takes 1.75 seconds
out of 2 seconds total to download, parse, and simplify
to 613 points (on Macbook air, home wifi network).
Implication:  Pre-processing GPX files is worthwhile. 

Version: pre-alpha, November 2014
Author: Michal Young  michal.young@gmail.com
"""

# Load modules from ./lib
# import sys
# sys.path.append("./lib")

import gpxpy
import gpxpy.gpx
import urllib.request
import cgi
import json

import cgitb; cgitb.enable()

def load(fname,delta=None):
    """
    Return a list of (lat, lon) pairs retrieved from a
    given gpx file. 

    Args:
       fname:  path of gpx file (relative or absolute)
       delta: If provided, simplify path to max deviation of delta meters.
              100 meters will generally simplify
              a RideWithGPS route radically.
    Returns:
        A list of (lattitude, longitude) pairs.
    """
    contents = open(fname, 'r', encoding="utf-8", errors="replace")
    gpx = gpxpy.parse(contents)
    if delta:
        gpx.simplify(delta)
    points = [ ]
    for track in gpx.tracks:
        for segment in track.segments:
            for point in segment.points:
                points.append( (point.latitude, point.longitude) )
    return points

print("Content-type: text/plain")
print("")

form = cgi.FieldStorage()
route = form.getvalue('route')
# route = "../fleche/Flechemen"
points = load(route + ".gpx")

print(json.dumps(points))
