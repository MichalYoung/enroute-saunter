#! /usr/local/bin/python3
# Redirect to Google map at last observed spotter point;
# hard-coded to Michal's feed for now 
# (experimental)
#
import spot  # Interface to Spot satellite tracker web service
import gmap

FEED_ID = "0GiLP5jn9iVj8z8qm90QaTnkpygdAmouk"  # Michal's shared page 

feed_msgs = spot.feed(FEED_ID)
points = [ spot.msg_to_latlon(pt) for pt in feed_msgs ]

print("Content-type: text/html\n\n")
print(gmap.MAP_DOC_PREFIX)
print(gmap.g_Marker(points[0]))
print(gmap.g_Polyline(points))
print(gmap.MAP_DOC_SUFFIX)

