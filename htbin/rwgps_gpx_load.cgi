#! /usr/bin/env python3
#
import gpx_load
import cgi
import json
# import cgitb; cgitb.enable()

form = cgi.FieldStorage()
route = form.getvalue('route')
if route:
    URL = "http://ridewithgps.com/routes/{}.gpx".format(route)
    points = gpx_load.feed(URL, delta=50)
else:
    points = [ ]

print("Content-type: text/plain")
print("")
print(json.dumps(points))


