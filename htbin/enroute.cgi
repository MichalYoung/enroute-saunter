#! /usr/bin/env python3
# Display map with RWGPS route and Spot tracks
# (experimental)
#
#  This version saves html to a file under a generated name,
#  so that it can be linked to. 
#
import cgi,cgitb # Before anything ... 
cgitb.enable() # Debugging on 

import os       # So we can look at environment variables 
import os.path  # to get template files relative to script location
import urllib.parse # to locate htbin relative to current url
import spot  # Interface to Spot satellite tracker web service
import gpx_load


print("Content-type: text/html\n\n")


try:
    whereami = os.environ["SCRIPT_FILENAME"]
    scriptdir = os.path.dirname(whereami)

    this_url = os.environ["REQUEST_URI"]
    url_parts = urllib.parse.urlparse(this_url)
    server = url_parts.scheme + url_parts.netloc + url_parts.path
    last_slash = server.rfind("/")
    server = server[:last_slash+1]
    print("<!-- Server will be {} -->".format(server))
except Exception: 
    scriptdir = "."
    server = "SERVER"
    print("<!-- CGI parameters not in environment -->")

#
# Form data
# 
form = cgi.FieldStorage()
if 'route' not in form or 'track' not in form: 
   print("<p>Form requires route# and at least one Spot track ID</p>")
   exit(1)
route = form.getvalue('route')
if ('name' in form):
    names = form.getlist("name")
else:
    names = [ ]
tracks = form.getlist("track")

if len(tracks) > len(names):
   names += ["???", "???", "???", "???"]

template = open("track_html.template")
for line in template:
    if line.lstrip().startswith("/*#SERVER#*/"):
        print('/*#SERVER#*/ var server="{}";'.format(server))
    elif line.lstrip().startswith("/*#ROUTE#*/"):
        print('/*#ROUTE#*/ var rwgps_route = "{}";'.format(route))
    elif line.lstrip().startswith("/*#FEEDS#*/"):
        print("/*#FEEDS#*/ ", end='')
        sep = ""
        for i in range(len(tracks)):
            print('{} {{ name: "{}", feed: "{}" }}'.format(sep,names[i], tracks[i]))
            sep = ","
    else:
        print(line.rstrip())



