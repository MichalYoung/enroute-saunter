#! /usr/bin/env python3
#
"""
Query Spot tracker service to refresh Spot track in MongoDB.
Designed to be run asynchronously using "os.system(...)"

Should not produce extra output! 

Usage: python3 spot_to_mongo.py trackID
"""
import sys
import json
from time import sleep
#
import spot
import time
# 
from config_local import MONGO_URL
import datetime

sys.path.append("./lib")
from pymongo import MongoClient

time_format = "%Y-%m-%d %H:%M:%S%z"
feedlist = sys.argv[1:]

# print("Opening MongoClient at {}".format(MONGO_URL))
client = MongoClient(MONGO_URL)
# print("Opened ... switching to client.tracks.samples collection")
db = client.tracks
collection = db.samples

now = datetime.datetime.now(datetime.timezone.utc)
nowstring = now.strftime(time_format)

pause = 0 # seconds, for sleep

for feed in feedlist:
   time.sleep(pause)
   request = { "id": feed }
   # print("Query for {}".format(request))
   record = collection.find_one(request)
   # print("Result of query: {}".format(record))
   if (record == None):
                record = { "id": feed,
                            "last_query_time": nowstring,
                            "messages": [ ]
                                  }
                collection.insert(record)
                # print("No record for {}".format(feed))
   messages = spot.feed(feed)
   # print("Spot observation: {}".format(messages))
   if len(messages) >= 0:
        collection.update_one( {"id": feed },
                                {"$set": { "messages": messages,
					   "last_query_time": nowstring }})
        # print("Mongo updated feed {} with timestame {}".format(feed,nowstring))
   pause = 4
        
                
                

