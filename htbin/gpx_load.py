"""
Retrieval of a list of points from a URL,
with optional simplification.

Performance note:  With Ride With GPS as data source,
dominant cost is download of raw GPX data.  For a GPX
route with 3594 points, download takes 1.75 seconds
out of 2 seconds total to download, parse, and simplify
to 613 points (on Macbook air, home wifi network).
Implication:  Pre-processing GPX files is worthwhile. 

Version: pre-alpha, November 2014
Author: Michal Young  michal.young@gmail.com
"""

# Load modules from ./lib
import sys
sys.path.append("./lib")

import gpxpy
import gpxpy.gpx
import urllib.request
import codecs


def feed(url,delta=None):
    """
    Return a list of (lat, lon) pairs retrieved from a
    given URL.  For example, URL could be

    Args:
       url:  full URL from which to fetch a GPX file
             Example: http://ridewithgps.com/routes/3616603
       delta: If provided, simplify path to max deviation of delta meters.
              100 meters will generally simplify
              a RideWithGPS route radically.
    Returns:
        A list of (lattitude, longitude) pairs.
    """
    gpx_response = urllib.request.urlopen(url)
    text = codecs.iterdecode(gpx_response,"UTF-8")
    # gpxpy.parse does not interact well with either the
    # utf-8 'bytes' stream, nor the interator from codecs.iterdecode,
    # so we construct a text object and parse that. 
    lines = [ ]
    for line in text:
        lines.append(line)
    contents = "".join(lines)
    gpx = gpxpy.parse(contents)
    if delta:
        gpx.simplify(delta)
    points = [ ]
    for track in gpx.tracks:
        for segment in track.segments:
            for point in segment.points:
                points.append( (point.latitude, point.longitude) )
    return points

