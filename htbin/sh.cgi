#! /bin/sh 
# 
echo "Content-type: text/plain"
echo ""
echo "Executing in shell environment"
env
echo "--------"
echo "Who am I, anyway?"
whoami
echo "What's the env of env"
/usr/bin/env env
/usr/bin/env sh <<END
echo "Executing nested shell"
END
echo "After nested shell"
/usr/bin/env python3  <<END
print("Running nested Python job")
exit(0)
END
echo "After nested Python"

echo "Trying external Python script"
/usr/bin/env python3 cgitest.py
echo "After external Python script"

