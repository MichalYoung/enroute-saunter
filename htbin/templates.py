"""
Code destined for spotter project, but currently in development. 
"""

def fill_template(template_file, fields, output):
    """
    Replace lines that start with /*#key*/ where 'key' indicates which 
    field we are replacing.  
    Args: 
       template_file:  A file (already open) from which to read the template.
       fields:  A dict mapping keys to values. Currently limited to a single replacement
       output:  An open file to which output is written. 
    """
						
