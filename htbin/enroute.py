#! /usr/local/bin/python3
# Redirect to Google map at last observed spotter point,
# with RWGPS route as background
# (experimental)
#
import cgi,cgitb
import spot  # Interface to Spot satellite tracker web service
import gpx_load
import gmap

cgitb.enable() # Debugging on 
#
# Form data
# 
form = cgi.FieldStorage()
track = form.getvalue('track')
route = form.getvalue('route')

rwgps_url = "http://ridewithgps.com/routes/" + route.strip() + ".gpx"

print("Content-type: text/html\n\n")
print(gmap.MAP_DOC_PREFIX)

# GPX route 
points = gpx_load.feed(rwgps_url, delta=50)
print(gmap.g_Polyline(points,color="#00FF00"))

# SPOT trace
feed_msgs = spot.feed(track)
points = [ spot.msg_to_latlon(pt) for pt in feed_msgs ]
print(gmap.g_Marker(points[0]))
print(gmap.g_Polyline(points))


print(gmap.MAP_DOC_SUFFIX)



print(gmap.MAP_DOC_SUFFIX)


